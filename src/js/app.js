import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  // Create and append img div to the main div
  const mainDiv = document.querySelector('div', 'main');
  const imgDiv = document.createElement('div');
  imgDiv.className = 'image';
  mainDiv.append(imgDiv);
  
  // Create and append img tag to the img div
  const img = document.createElement('img');
  img.src = 'http://localhost:8080/images/favicon.ico';
  imgDiv.append(img);

  // Select the image and on click double its size
  const imgDivQuery = document.querySelector('div', 'image').lastChild;
  imgDivQuery.addEventListener('click', function(){
    imgDivQuery.style.transform = 'scale(2,2)';
  })
});
